import React, { FC } from 'react';
import { navigate } from '@reach/router';

import { PageType } from "../../types/page";
import { Person } from "../../types/person";

import Loader from "../../components/loader/loader.component";
import PeopleComponent from "../../components/people/people.component";
import Pagination from "../../components/pagination/pagination.component";

import { PeopleConsumer } from "../../contexts/provider";

const PeopleContainer: FC<PageType> = () => {
  return (
      <PeopleConsumer>
        {({ loading, people, count }: any) => {
          return (
            <>
              {loading ? (
                <div className="loader">
                  <Loader />
                </div>
              ) : (
                <div className="people-container">
                  {people?.people.map((person: Person) => {

                    return (<div onClick={() => navigate(`/details/${person.name}`)}>
                      <PeopleComponent key={person.name} person={person} />
                    </div>)
                })}
                </div>
              )}
              <div className="pagination">
                <Pagination
                  next={people?.next}
                  previous={people?.previous}
                  count={count}
                />
              </div>
            </>
          );
        }}
      </PeopleConsumer>
  );
};

export default PeopleContainer;
