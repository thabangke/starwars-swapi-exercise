import React, { FC } from "react";
import { navigate } from "@reach/router";
import { useLocation } from "@reach/router";

import { PageType } from "../../types/page";
import { Person } from "../../types/person";

import Loader from "../../components/loader/loader.component";
import PersonDetailStyles from "../../components/person/person.styled";
import PersonComponent from "../../components/person/person.component";

import useGetPerson from "../../hooks/person.hooks";

const PersonContainer: FC<PageType> = () => {
  const location = useLocation();
  const name = location.pathname.split("/").pop();

  const { loading, error, data }: any = useGetPerson(name);

  const person: Person = data?.getPerson[0];

  return (
    <>
      <div className="back">
          <button onClick={() => navigate(-1)}>&#10229; back</button>
        </div>
        
        {!loading && (
          <h1>
            <span>{person.name}</span>
          </h1>
        )}

      <PersonDetailStyles>
        <div className={`details-container ${loading ? "center" : ""}`}>
          {loading && !error ? <Loader /> : <PersonComponent person={person} />}
        </div>
      </PersonDetailStyles>
    </>
  );
};

export default PersonContainer;
