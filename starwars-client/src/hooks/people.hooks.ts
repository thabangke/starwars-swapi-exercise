import { useQuery, gql } from "@apollo/client";

const GET_PEOPLE = gql`
  query GET_PEOPLE($page: Int!) {
    getAllPeople(page: $page) {
      count
      next
      previous
      people {
        name
        height
        mass
        gender
        homeworld
      }
    }
  }
`;

const useGetPeople = (page: any) => {
  return useQuery(GET_PEOPLE, {
    variables: {
      page,
    },
  });
};

export default useGetPeople;
