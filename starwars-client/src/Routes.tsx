import React, { FC } from 'react';
import { Router, RouteComponentProps } from '@reach/router';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';

import PeopleContainer from './containers/people/people.container';
import Navbar from './components/navbar/navbar.component';
import PersonContainer from './containers/person/person.container';

import Container from './components/common/containter.styled';

import { PeopleProvider } from './contexts/provider';


const client = new ApolloClient({
  uri: "Server is running on http://localhost:8000/graphql",
  cache: new InMemoryCache(),
});

const appRouter: FC<RouteComponentProps> = () => (
  <ApolloProvider client={client}>
    <Navbar />
    <Container>
      <Router>
        <PeopleProvider path="/">
          <PeopleContainer path="/" />
          <PersonContainer path="/details/:name" />
        </PeopleProvider>
      </Router>
    </Container>
  </ApolloProvider>
);

export default appRouter;
