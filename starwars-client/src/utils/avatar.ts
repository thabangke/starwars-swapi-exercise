
export const avatar = (name: string) => {
  let fullname: Array<string> = name.split(" ");

  return (
    `${fullname[0].charAt(0)}` +
    `${fullname.length > 1 ? fullname[1].charAt(0) : ""}`
  );
};