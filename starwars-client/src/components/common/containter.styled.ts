import styled from "styled-components";

const Container = styled.div`
  width: 90%;
  margin: 0 auto;

  padding-top: 200px;

  .people-container {
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    margin-top: 25px;
    cursor: pointer;
  }

  .pagination {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .back {

    button {
      color: #3b3be6;
      cursor: pointer;
      padding: 10px;
      width: fit-content;
      transition: all 0.3s ease-in;
      border: none;

      &:hover {
        text-decoration: underline;
        transition: all 0.3s ease-in;
      }
    }
  }
`;

export default Container;
