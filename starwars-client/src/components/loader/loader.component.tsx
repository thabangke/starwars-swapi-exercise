import React, { FC } from "react";

import StyledLoader from "./loader.styled";

const Pagination: FC = () => {
  return (
    <StyledLoader>
      <div className="loader">
        <div className="dot"></div>
        <div className="dot"></div>
        <div className="dot"></div>
      </div>
    </StyledLoader>
  );
};

export default Pagination;
