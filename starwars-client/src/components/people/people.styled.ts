import styled from "styled-components";

const StyledPeople = styled.div`
  display: flex;
  min-width: fit-content;
  width: 360px;
  background: #fff;
  margin-bottom: 15px;
  padding: 10px;
  box-shadow: rgb(0 0 0 / 7%) 0px 5px 45px;
  border-radius: 6px;
  margin-right: 15px;
  transition: all 0.3s ease-in;

  &:hover {
    transition: all 0.3s ease-in;
  }

  .person-details {
    display: flex;
    flex-direction: column;
    margin-left: 15px;
    align-items: ;
    justify-content: center;
  }

  .person-initial {
    width: 45px;
    height: 45px;
    border-radius: 50%;
    border: 1px solid #ffc700;
    background-color: #fff8dd;
    color: #ffc700;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 18px;
    font-weight: 600;
  }

  .person-name {
    font-size: 16px;
  }

  .person-home {
    color: #888787;
    font-size: 12px;
    padding-top: 8px;
    font-weight: 500;
  }
`;

export default StyledPeople;
