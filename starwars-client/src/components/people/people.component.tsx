import React, { FC } from 'react';

import { Person } from "../../types/person";

import { avatar } from "../../utils/avatar";

import StyledPeople from "./people.styled";

interface IPerson {
  person: Person;
}

const PeopleComponent: FC<IPerson> = ({ person: { name, homeworld } }) => {
  return (
    <StyledPeople>
      <div className="person-initial">{avatar(name)}</div>
      <div className="person-details">
        <div className="person-name">
          <strong>{name}</strong>
        </div>
        <p className="person-home">{homeworld}</p>
      </div>
    </StyledPeople>
  );
};

export default PeopleComponent;
