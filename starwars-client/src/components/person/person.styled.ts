import styled from "styled-components";

const PersonDetailStyles = styled.div`

  margin-top: 20px;

  .details-container {
    display: flex;
    background: #fff;
  }

  .details {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
  }

  .avatar-container {
    display: flex;
    padding: 10px;
    margin-right: 15px;
  }

  .person-initial {
    width: 90px;
    height: 90px;
    border-radius: 50%;
    border: 1px solid #ffc700;
    background-color: #fff8dd;
    color: #ffc700;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 18px;
    font-weight: 600;
  }

  .person-details {
    display: flex;
    flex-direction: column;
    margin-left: 15px;
    align-items: ;
    justify-content: center;
    font-weight: 600;

    p{
      margin-bottom: 5px;
    }
  }

  .person-avatar {
    width: 90px;
    height: 90px;
    border-radius: 50%;
    border: 1px solid #ffc700;
    background-color: #fff8dd;
    color: #ffc700;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 18px;
    font-weight: 600;
  }

  .person-name {
    font-size: 16px;
  }
`;

export default PersonDetailStyles;
