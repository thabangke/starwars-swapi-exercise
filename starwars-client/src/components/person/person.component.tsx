import React, { FC } from 'react';

import { Person } from "../../types/person";

import { avatar } from "../../utils/avatar";

interface IPerson {
  person: Person;
}

const PersonComponent: FC<IPerson> = ({ person }) => {
  return (
    <>
      <div className="avatar-container">
        <div className="person-initial">{avatar(person.name)}</div>
      </div>
      <div className="person-details">
          <p><b>Height:</b> {person.height}</p>
          <p><b>Gender:</b> {person.gender}</p> 
          <p><b>Mass:</b> {person.mass}</p>  
          <p><b>Home world:</b> {person.homeworld}</p>  
      </div>
    </>
  );
};

export default PersonComponent;
