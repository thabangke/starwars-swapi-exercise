import styled from "styled-components";

const StyledPagination = styled.div`
  font-size: 10px;
  font-weight: 500;
  text-align: center;
  display: inline-grid;
  grid-template-columns: repeat(4, auto);
  align-items: stretch;
  justify-content: center;
  align-content: center;
  margin: 2rem 0;
  border-radius: 10px;

  & > * {
    margin: 0;
    padding: 15px 30px;
  }

  div[aria-disabled="true"] {
    color: grey;
    pointer-events: none;
  }

  div[aria-disabled="false"] {
    cursor: pointer;
  }
`;

export default StyledPagination;
