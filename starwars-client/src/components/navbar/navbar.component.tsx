import React, { FC } from 'react';

import StarwarsIcon from '../../assets/logo.png';

import StyledNavbar from './navabar.styled';

const Navbar: FC = () => {
  return (
    <StyledNavbar>
      <img src={ StarwarsIcon } width="120" alt="" />
    </StyledNavbar>
  );
};

export default Navbar;
