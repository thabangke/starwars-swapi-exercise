import styled from "styled-components";

const StyledNavbar = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: .75rem 20px;

  background-color: #272b33;
  box-shadow: 0 1rem 3rem rgba(0,0,0,.175)!important;

  width: 100%;
  z-index: 1000;

  a {
      font-size: 1.25rem;
      text-decoration: none;
      white-space: nowrap;
  }
`;

export default StyledNavbar;
