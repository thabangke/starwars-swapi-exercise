import { ReactNode } from "react";

export type PageType = {
  path?: string;
  location?: LocationType;
  children?: Array<ReactNode>;
  default?: boolean;
};

export type LocationType = {
  pathname: string;
  url?: string;
  search?: string;
};
