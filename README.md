# starwars-swapi-exercise

## dependencies required

- [node](https://nodejs.org/en/)

## installation

```bash
clone
git clone https://thabangke@bitbucket.org/thabangke/starwars-swapi-exercise.git

cd starwars-server
npm install

cd starwars-client
npm install

```

## run

```bash
starwars-server
npm start

starwars-client
npm start
```

## run build

```bash
npm run build
```

## access

```bash
starwars-server
localhost:8000

starwars-client
localhost:3000
```
