"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _taggedTemplateLiteral2 = _interopRequireDefault(require("@babel/runtime/helpers/taggedTemplateLiteral"));

var _apolloServerExpress = require("apollo-server-express");

var _templateObject;

var typeDefs = (0, _apolloServerExpress.gql)(_templateObject || (_templateObject = (0, _taggedTemplateLiteral2["default"])(["\n  type Query {\n    getPeople(page: Int!): People\n    getPerson(name: String!): [Person]\n  }\n\n  type People {\n    count: Int\n    next: String\n    previous: String\n    people: [Person]\n  }\n\n  type Person {\n    name: String\n    height: String\n    mass: String\n    gender: String\n    homeworld: String\n  }\n"])));
var _default = typeDefs;
exports["default"] = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zY2hlbWFzL3Blb3BsZS50cyJdLCJuYW1lcyI6WyJ0eXBlRGVmcyIsImdxbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7OztBQUVBLElBQU1BLFFBQVEsT0FBR0Msd0JBQUgsMFpBQWQ7ZUFzQmVELFEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBncWwgfSBmcm9tICdhcG9sbG8tc2VydmVyLWV4cHJlc3MnO1xuXG5jb25zdCB0eXBlRGVmcyA9IGdxbGBcbiAgdHlwZSBRdWVyeSB7XG4gICAgZ2V0UGVvcGxlKHBhZ2U6IEludCEpOiBQZW9wbGVcbiAgICBnZXRQZXJzb24obmFtZTogU3RyaW5nISk6IFtQZXJzb25dXG4gIH1cblxuICB0eXBlIFBlb3BsZSB7XG4gICAgY291bnQ6IEludFxuICAgIG5leHQ6IFN0cmluZ1xuICAgIHByZXZpb3VzOiBTdHJpbmdcbiAgICBwZW9wbGU6IFtQZXJzb25dXG4gIH1cblxuICB0eXBlIFBlcnNvbiB7XG4gICAgbmFtZTogU3RyaW5nXG4gICAgaGVpZ2h0OiBTdHJpbmdcbiAgICBtYXNzOiBTdHJpbmdcbiAgICBnZW5kZXI6IFN0cmluZ1xuICAgIGhvbWV3b3JsZDogU3RyaW5nXG4gIH1cbmA7XG5cbmV4cG9ydCBkZWZhdWx0IHR5cGVEZWZzO1xuIl19