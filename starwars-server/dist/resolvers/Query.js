"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _axios = _interopRequireDefault(require("axios"));

var getBaseURL = function getBaseURL() {
  return 'https://swapi.dev/api';
};

var Query = {
  getPeople: function () {
    var _getPeople = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_, _ref) {
      var page, response, data;
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              page = _ref.page;
              _context.next = 3;
              return _axios["default"].get("".concat(getBaseURL, "/people/?page=").concat(page));

            case 3:
              response = _context.sent.data;
              data = {
                count: response.count,
                next: response.next,
                previous: response.previous,
                people: response.results
              };
              return _context.abrupt("return", data);

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function getPeople(_x, _x2) {
      return _getPeople.apply(this, arguments);
    }

    return getPeople;
  }(),
  getPerson: function () {
    var _getPerson = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(_, _ref2) {
      var name, person;
      return _regenerator["default"].wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              name = _ref2.name;
              _context2.next = 3;
              return _axios["default"].get("".concat(getBaseURL, "/people/?search=").concat(name));

            case 3:
              person = _context2.sent.data.results;
              return _context2.abrupt("return", person);

            case 5:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function getPerson(_x3, _x4) {
      return _getPerson.apply(this, arguments);
    }

    return getPerson;
  }()
};
var _default = Query;
exports["default"] = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9yZXNvbHZlcnMvcXVlcnkudHMiXSwibmFtZXMiOlsiZ2V0QmFzZVVSTCIsIlF1ZXJ5IiwiZ2V0UGVvcGxlIiwiXyIsInBhZ2UiLCJheGlvcyIsImdldCIsInJlc3BvbnNlIiwiZGF0YSIsImNvdW50IiwibmV4dCIsInByZXZpb3VzIiwicGVvcGxlIiwicmVzdWx0cyIsImdldFBlcnNvbiIsIm5hbWUiLCJwZXJzb24iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQSxJQUFNQSxVQUFVLEdBQUcsU0FBYkEsVUFBYSxHQUFNO0FBQ3ZCLFNBQU8sdUJBQVA7QUFDRCxDQUZEOztBQUlBLElBQU1DLEtBQUssR0FBRztBQUNaQyxFQUFBQSxTQUFTO0FBQUEsbUdBQUUsaUJBQU9DLENBQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWlCQyxjQUFBQSxJQUFqQixRQUFpQkEsSUFBakI7QUFBQTtBQUFBLHFCQUNlQyxrQkFBTUMsR0FBTixXQUFhTixVQUFiLDJCQUF3Q0ksSUFBeEMsRUFEZjs7QUFBQTtBQUNIRyxjQUFBQSxRQURHLGlCQUNnRUMsSUFEaEU7QUFHSEEsY0FBQUEsSUFIRyxHQUdJO0FBQ1hDLGdCQUFBQSxLQUFLLEVBQUVGLFFBQVEsQ0FBQ0UsS0FETDtBQUVYQyxnQkFBQUEsSUFBSSxFQUFFSCxRQUFRLENBQUNHLElBRko7QUFHWEMsZ0JBQUFBLFFBQVEsRUFBRUosUUFBUSxDQUFDSSxRQUhSO0FBSVhDLGdCQUFBQSxNQUFNLEVBQUVMLFFBQVEsQ0FBQ007QUFKTixlQUhKO0FBQUEsK0NBVUZMLElBVkU7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBRjs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSxLQURHO0FBY1pNLEVBQUFBLFNBQVM7QUFBQSxtR0FBRSxrQkFBT1gsQ0FBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUJZLGNBQUFBLElBQWpCLFNBQWlCQSxJQUFqQjtBQUFBO0FBQUEscUJBQ2FWLGtCQUFNQyxHQUFOLFdBQWFOLFVBQWIsNkJBQTBDZSxJQUExQyxFQURiOztBQUFBO0FBQ0hDLGNBQUFBLE1BREcsa0JBQ2dFUixJQURoRSxDQUVOSyxPQUZNO0FBQUEsZ0RBSUZHLE1BSkU7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBRjs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQWRHLENBQWQ7ZUFzQmVmLEsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnO1xuXG5jb25zdCBnZXRCYXNlVVJMID0gKCkgPT4ge1xuICByZXR1cm4gJ2h0dHBzOi8vc3dhcGkuZGV2L2FwaSc7XG59XG5cbmNvbnN0IFF1ZXJ5ID0ge1xuICBnZXRQZW9wbGU6IGFzeW5jIChfOiBhbnksIHsgcGFnZSB9OiBhbnkpID0+IHtcbiAgICBjb25zdCByZXNwb25zZSA9IChhd2FpdCBheGlvcy5nZXQoYCR7Z2V0QmFzZVVSTH0vcGVvcGxlLz9wYWdlPSR7cGFnZX1gKSkuZGF0YTtcblxuICAgIGNvbnN0IGRhdGEgPSB7XG4gICAgICBjb3VudDogcmVzcG9uc2UuY291bnQsXG4gICAgICBuZXh0OiByZXNwb25zZS5uZXh0LFxuICAgICAgcHJldmlvdXM6IHJlc3BvbnNlLnByZXZpb3VzLFxuICAgICAgcGVvcGxlOiByZXNwb25zZS5yZXN1bHRzLFxuICAgIH07XG5cbiAgICByZXR1cm4gZGF0YTtcbiAgfSxcblxuICBnZXRQZXJzb246IGFzeW5jIChfOiBhbnksIHsgbmFtZSB9OiBhbnkpID0+IHtcbiAgICBjb25zdCBwZXJzb24gPSAoYXdhaXQgYXhpb3MuZ2V0KGAke2dldEJhc2VVUkx9L3Blb3BsZS8/c2VhcmNoPSR7bmFtZX1gKSkuZGF0YVxuICAgICAgLnJlc3VsdHM7XG5cbiAgICByZXR1cm4gcGVyc29uO1xuICB9LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgUXVlcnk7XG4iXX0=