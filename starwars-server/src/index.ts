
import express from 'express';
import { ApolloServer } from 'apollo-server-express';

import Query from './resolvers/query';
import typeDefs from './schemas/people';

const PORT = process.env.PORT || 8000;
const app = express();

const resolvers = { Query };

const server = new ApolloServer({ typeDefs, resolvers });

server.applyMiddleware({ app });

app.listen(PORT, () =>
  console.log(
    `Server is running on http://localhost:${PORT}` + server.graphqlPath
  )
);
