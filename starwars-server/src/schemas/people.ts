import { gql } from 'apollo-server-express';

const typeDefs = gql`
  type Query {
    getPeople(page: Int!): People
    getPerson(name: String!): [Person]
  }

  type People {
    count: Int
    next: String
    previous: String
    people: [Person]
  }

  type Person {
    name: String
    height: String
    mass: String
    gender: String
    homeworld: String
  }
`;

export default typeDefs;
