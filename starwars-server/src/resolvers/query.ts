import axios from 'axios';

const getBaseURL = () => {
  return 'https://swapi.dev/api';
}

const Query = {
  getPeople: async (_: any, { page }: any) => {
    const response = (await axios.get(`${getBaseURL}/people/?page=${page}`)).data;

    const data = {
      count: response.count,
      next: response.next,
      previous: response.previous,
      people: response.results,
    };

    return data;
  },

  getPerson: async (_: any, { name }: any) => {
    const person = (await axios.get(`${getBaseURL}/people/?search=${name}`)).data
      .results;

    return person;
  },
};

export default Query;
